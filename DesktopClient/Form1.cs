﻿using System;
using System.Windows.Forms;
using DesktopClient.SecondService;

namespace DesktopClient
{
    public partial class Form1 : Form
    {

        private readonly SecondServiceClient client = new SecondServiceClient();
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = client.GetAllOrders();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = client.GetAllOrders();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var name = textBox1.Text;
            var city = textBox2.Text;
            var country = textBox3.Text;
            if (string.IsNullOrEmpty(country) || string.IsNullOrEmpty(name)|| string.IsNullOrEmpty(city))
            {
                MessageBox.Show("Fill all properties");
                return;
            }
            var model = new Orders
            {
                NAME = name,
                CITY = city,
                COUNTRY = country
            };
            client.CreateOrder(model);
            dataGridView1.DataSource = client.GetAllOrders();
        }

        private void button2_Click(object sender, EventArgs e)
        {   
            var selectedrows = dataGridView1.SelectedRows;
            if (selectedrows.Count == 0)
            {
                MessageBox.Show("No rows selected");
            }
            foreach (DataGridViewRow selectedrow in selectedrows)
            {
                var id = int.Parse(selectedrow.Cells[2].Value.ToString());
                client.DeleteOrder(id);
            }
            dataGridView1.DataSource = client.GetAllOrders();
        }

        private void dataGridView1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (sender is DataGridView grid)
            {
                var city = grid[0, e.RowIndex].Value.ToString();
                var country = grid[1, e.RowIndex].Value.ToString();
                var id = int.Parse(grid[2, e.RowIndex].Value.ToString());
                var name = grid[3, e.RowIndex].Value.ToString();
                var model = new Orders
                {
                    CITY = city,
                    COUNTRY = country,
                    ID = id,
                    NAME = name
                };
                client.EditOrder(model);
                dataGridView1.DataSource = client.GetAllOrders();
            }

        }

    }
}
