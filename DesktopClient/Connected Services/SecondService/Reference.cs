﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DesktopClient.SecondService {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Orders", Namespace="http://schemas.datacontract.org/2004/07/SecondWebService")]
    [System.SerializableAttribute()]
    public partial class Orders : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string CITYField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string COUNTRYField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int IDField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string NAMEField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string CITY {
            get {
                return this.CITYField;
            }
            set {
                if ((object.ReferenceEquals(this.CITYField, value) != true)) {
                    this.CITYField = value;
                    this.RaisePropertyChanged("CITY");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string COUNTRY {
            get {
                return this.COUNTRYField;
            }
            set {
                if ((object.ReferenceEquals(this.COUNTRYField, value) != true)) {
                    this.COUNTRYField = value;
                    this.RaisePropertyChanged("COUNTRY");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int ID {
            get {
                return this.IDField;
            }
            set {
                if ((this.IDField.Equals(value) != true)) {
                    this.IDField = value;
                    this.RaisePropertyChanged("ID");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string NAME {
            get {
                return this.NAMEField;
            }
            set {
                if ((object.ReferenceEquals(this.NAMEField, value) != true)) {
                    this.NAMEField = value;
                    this.RaisePropertyChanged("NAME");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="SecondService.ISecondService")]
    public interface ISecondService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISecondService/FindOrder", ReplyAction="http://tempuri.org/ISecondService/FindOrderResponse")]
        DesktopClient.SecondService.Orders FindOrder(int id);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISecondService/FindOrder", ReplyAction="http://tempuri.org/ISecondService/FindOrderResponse")]
        System.Threading.Tasks.Task<DesktopClient.SecondService.Orders> FindOrderAsync(int id);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISecondService/GetAllOrders", ReplyAction="http://tempuri.org/ISecondService/GetAllOrdersResponse")]
        DesktopClient.SecondService.Orders[] GetAllOrders();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISecondService/GetAllOrders", ReplyAction="http://tempuri.org/ISecondService/GetAllOrdersResponse")]
        System.Threading.Tasks.Task<DesktopClient.SecondService.Orders[]> GetAllOrdersAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISecondService/DeleteOrder", ReplyAction="http://tempuri.org/ISecondService/DeleteOrderResponse")]
        bool DeleteOrder(int id);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISecondService/DeleteOrder", ReplyAction="http://tempuri.org/ISecondService/DeleteOrderResponse")]
        System.Threading.Tasks.Task<bool> DeleteOrderAsync(int id);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISecondService/EditOrder", ReplyAction="http://tempuri.org/ISecondService/EditOrderResponse")]
        bool EditOrder(DesktopClient.SecondService.Orders client);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISecondService/EditOrder", ReplyAction="http://tempuri.org/ISecondService/EditOrderResponse")]
        System.Threading.Tasks.Task<bool> EditOrderAsync(DesktopClient.SecondService.Orders client);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISecondService/CreateOrder", ReplyAction="http://tempuri.org/ISecondService/CreateOrderResponse")]
        bool CreateOrder(DesktopClient.SecondService.Orders order);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISecondService/CreateOrder", ReplyAction="http://tempuri.org/ISecondService/CreateOrderResponse")]
        System.Threading.Tasks.Task<bool> CreateOrderAsync(DesktopClient.SecondService.Orders order);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface ISecondServiceChannel : DesktopClient.SecondService.ISecondService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class SecondServiceClient : System.ServiceModel.ClientBase<DesktopClient.SecondService.ISecondService>, DesktopClient.SecondService.ISecondService {
        
        public SecondServiceClient() {
        }
        
        public SecondServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public SecondServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public SecondServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public SecondServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public DesktopClient.SecondService.Orders FindOrder(int id) {
            return base.Channel.FindOrder(id);
        }
        
        public System.Threading.Tasks.Task<DesktopClient.SecondService.Orders> FindOrderAsync(int id) {
            return base.Channel.FindOrderAsync(id);
        }
        
        public DesktopClient.SecondService.Orders[] GetAllOrders() {
            return base.Channel.GetAllOrders();
        }
        
        public System.Threading.Tasks.Task<DesktopClient.SecondService.Orders[]> GetAllOrdersAsync() {
            return base.Channel.GetAllOrdersAsync();
        }
        
        public bool DeleteOrder(int id) {
            return base.Channel.DeleteOrder(id);
        }
        
        public System.Threading.Tasks.Task<bool> DeleteOrderAsync(int id) {
            return base.Channel.DeleteOrderAsync(id);
        }
        
        public bool EditOrder(DesktopClient.SecondService.Orders client) {
            return base.Channel.EditOrder(client);
        }
        
        public System.Threading.Tasks.Task<bool> EditOrderAsync(DesktopClient.SecondService.Orders client) {
            return base.Channel.EditOrderAsync(client);
        }
        
        public bool CreateOrder(DesktopClient.SecondService.Orders order) {
            return base.Channel.CreateOrder(order);
        }
        
        public System.Threading.Tasks.Task<bool> CreateOrderAsync(DesktopClient.SecondService.Orders order) {
            return base.Channel.CreateOrderAsync(order);
        }
    }
}
