﻿using System.Web.Mvc;
using WebClient.MainService;

namespace WebClient.Controllers
{
    public class OrderController : Controller
    {
        private readonly MainServiceClient client = new MainServiceClient(); 
        // GET: Order
        public ActionResult Index()
        {
            var model = client.GetAllOrders();
            return View(model);
        }

        // GET: Order/Details/5
        public ActionResult Details(int id)
        {
            var model = client.FindOrder(id);
            return View(model);
        }

        // GET: Order/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Order/Create
        [HttpPost]
        public ActionResult Create(Orders order)
        {
            try
            {
                client.CreateOrder(order);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Order/Edit/5
        public ActionResult Edit(int id)
        {
            var model = client.FindOrder(id);
            return View(model);
        }

        // POST: Order/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Orders order)
        {
            try
            {
                client.EditOrder(order);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Order/Delete/5
        public ActionResult Delete(int id)
        {
            var model = client.FindOrder(id);
            return View(model);
        }

        // POST: Order/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                client.DeleteOrder(id);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
