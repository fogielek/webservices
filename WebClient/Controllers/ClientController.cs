﻿using System.Web.Mvc;
using WebClient.MainService;

namespace WebClient.Controllers
{
    public class ClientController : Controller
    {
        private readonly MainServiceClient client = new MainServiceClient();
        // GET: Client
        public ActionResult Index()
        {
            var model = client.GetAllClinets();
            return View(model);
        }

        // GET: Client/Details/5
        public ActionResult Details(int id)
        {
            var item = client.FindClient(id);
            return View(item);
        }

        //GET: Client/Create
        public ActionResult Create()
        {
            return View();

        }

        // POST: Client/Create
        [HttpPost]
        public ActionResult Create(Clients item)
        {
            try
            {
                client.CreateClient(item);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Client/Edit/5
        public ActionResult Edit(int id)
        {
            var model = client.FindClient(id);

            return View(model);
        }

        // POST: Client/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Clients item)
        {
            try
            {
                client.EditClient(item);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Client/Delete/5
        public ActionResult Delete(int id)
        {
            var item = client.FindClient(id);
            return View(item);
        }

        // POST: Client/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                client.DeleteClient(id);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
