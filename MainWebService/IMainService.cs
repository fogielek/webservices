﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace MainWebService
{
    [ServiceContract]
    public interface IMainService
    {
        [OperationContract]
        Task<Clients> FindClient(int id);

        [OperationContract]
        Task<List<Clients>> GetAllClinets();

        [OperationContract]
        bool DeleteClient(int id);

        [OperationContract]
        bool EditClient(Clients client);

        [OperationContract]
        bool CreateClient(Clients client);

        [OperationContract]
        Task<Orders> FindOrder(int id);

        [OperationContract]
        Task<List<Orders>> GetAllOrders();

        [OperationContract]
        bool DeleteOrder(int id);

        [OperationContract]
        bool EditOrder(Orders client);

        [OperationContract]
        bool CreateOrder(Orders order);
    }
}
