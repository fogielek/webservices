﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Threading.Tasks;

namespace MainWebService
{
    public class MainService : IMainService
    {
        private readonly MainContext _context = new MainContext();

        public async Task<Clients> FindClient(int id)
        {
            return await _context.Clients.FindAsync(id);
        }

        public async Task<List<Clients>> GetAllClinets()
        {
            return await _context.Clients.ToListAsync();
        }

        public bool DeleteClient(int id)
        {
            try
            {
                var recordToDelete = _context.Clients.Find(id);
                if (recordToDelete == null) return false;
                _context.Clients.Remove(recordToDelete);
                _context.SaveChanges();

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool EditClient(Clients client)
        {
            try
            {
                _context.Clients.AddOrUpdate(client);
                _context.SaveChanges();

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool CreateClient(Clients client)
        {
            try
            {
                _context.Clients.Add(client);
                _context.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public async Task<Orders> FindOrder(int id)
        {
            return await _context.Orders.FindAsync(id);
        }

        public async Task<List<Orders>> GetAllOrders()
        {
            return await _context.Orders.ToListAsync();
        }

        public bool DeleteOrder(int id)
        {
            try
            {
                var recordToDelete = _context.Orders.Find(id);
                if (recordToDelete == null) return false;
                _context.Orders.Remove(recordToDelete);
                _context.SaveChanges();

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool EditOrder(Orders order)
        {
            try
            {
                _context.Orders.AddOrUpdate(order);
                _context.SaveChanges();

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool CreateOrder(Orders order)
        {
            try
            {
                _context.Orders.Add(order);
                _context.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}
