﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Threading.Tasks;

namespace SecondWebService
{

    public class SecondService : ISecondService
    {
        private readonly SecondWebServiceEntities _context = new SecondWebServiceEntities();
        public async Task<Orders> FindOrder(int id)
        {
            return await _context.Orders.FindAsync(id);
        }

        public async Task<List<Orders>> GetAllOrders()
        {
            return await _context.Orders.ToListAsync();
        }

        public bool DeleteOrder(int id)
        {
            try
            {
                var recordToDelete = _context.Orders.Find(id);
                if (recordToDelete == null) return false;
                _context.Orders.Remove(recordToDelete);
                _context.SaveChanges();

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool EditOrder(Orders order)
        {
            try
            {
                _context.Orders.AddOrUpdate(order);
                _context.SaveChanges();

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool CreateOrder(Orders order)
        {
            try
            {
                _context.Orders.Add(order);
                _context.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}
