﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace SecondWebService
{

    [ServiceContract]
    public interface ISecondService
    {
        [OperationContract]
        Task<Orders> FindOrder(int id);

        [OperationContract]
        Task<List<Orders>> GetAllOrders();

        [OperationContract]
        bool DeleteOrder(int id);

        [OperationContract]
        bool EditOrder(Orders client);

        [OperationContract]
        bool CreateOrder(Orders order);
    }
}
